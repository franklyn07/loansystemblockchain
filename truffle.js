// Allows us to use ES6 in our migrations and tests.
require('babel-register')

module.exports = {
  networks: {
    development: {
      host: "localhost",
      port: 8545,
      network_id: "*" // Match any network id
    },
    rinkeby: {
      host: "localhost", // Connect to geth on the specified
      port: 8545,
      from: "0x6c4BFC1EB57b14057E62a7cDd9c3DDfaE30e38ED", 
      // default address to use for any transaction Truffle makes during migrations
     // network_id: 4,
      gas: 4612388 // Gas limit used for deploys
    }
  }
};
