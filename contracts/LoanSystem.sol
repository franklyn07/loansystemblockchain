pragma solidity ^0.4.24;

contract LoanSystem{

    //owner of LoanSystem
    address private owner;
    uint256 private contractFund;
    uint256 private transactionId;

    //struct that represents loan structure
    struct loan{
        address borrower;
        address funder;
        address guarantor;
        uint256 ether_2_borrow;
        uint256 ether_interest_lender;
        uint256 ether_interest_guarantor;
        bool isGuaranteed;
        uint256 deadline;
    }

    //struct that represents guarantee structure
    struct guarantee{
        address guarantor;
        address borrower;

        //loan id
        uint256 loanId;

        //ether depositied as guarantee = loan amount
        uint256 guarantee;
        //total amount of interest borrower offerred 
        uint256 total_interest;
        //amount of ether guarantor will be taking from interest offerred
        uint256 guarantor_interest;
    }

    //holds a copy of all the loan id which are unfunded right now
    uint256 [] private currentUnfundedLoans;

    //map between loandId and loan struct of unfunded loans
    mapping(uint256 => loan) private unfundedLoans;

    //map between loanId and loan struct of funded Loans
    mapping(uint256 => loan) private fundedLoans;

    //mapping between a borrower and all his loans
    mapping(address => uint256[]) private loansBorrower;

    //mapping between a lender and all the loans he funded 
    mapping(address => uint256[]) private loansFunder;

    //mapping between a guarantor and all the loans he guaranteed
    mapping(address => uint256[]) private loansGuarantor;
    
    //mapping between a loan and all guarantees proposed for it
    mapping(uint256 => uint256[]) private guaranteesProposed;

    //mapping between guarantee id and guarantee
    mapping(uint256 => guarantee) private guaranteeReceipt;

    //constructor of system
    constructor()public{
        owner = msg.sender;
        contractFund = 0;
        transactionId = 0;
    }

    //--Modifiers--

    //checks that guarantee details are valid
    modifier isGuaranteeValid(uint256 loanId, uint256 etherInInterest){
        loan storage loanObject = unfundedLoans[loanId];

        //check loan is unfunded if it isn't (not in unfundedloans map) return 0 
        require(loanObject.borrower != 0, "Loan Already Funded");

        //check loan is not guaranteed yet
        require(loanObject.isGuaranteed == false && loanObject.guarantor ==0, "Loan Already Guaranteed");

        //check amount of ether desired in interest is less than the one offerred by borrower
        require(loanObject.ether_interest_lender > etherInInterest, "Interest Rate Too High");

        //check loan is not old - overdue
        require(loanObject.deadline > now, "Loan Overdue");

        //check amount of ether sent actually covers loan
        require(loanObject.ether_2_borrow <= msg.value, "Note Enough Funds Sent");
        _;
    }

    //checks that client trying to access data is owner of that data
    //note that functions that just view data have no need of checking if owner
    //this is only useful when data will be modified
    modifier isOwnerOfLoan(uint256 loanId){
        address loanOwner;
        if (unfundedLoans[loanId].borrower != 0){
            loanOwner =  unfundedLoans[loanId].borrower;
        }else{
            loanOwner = fundedLoans[loanId].borrower;
        }
        require(msg.sender == loanOwner, "Unauthorised Access");
        _;
    }

    //checks that the borrower is the loan owner and that due date is not over
    //it also checks that amount is enough
    modifier isPaybackValid(uint256 loanId){
        loan storage loanObject = fundedLoans[loanId];
        require(loanObject.borrower != 0, "Loan is Either Paid Back Or Not Funded.");
        require(loanObject.deadline > now, "Loan Was Overdue - Necessary Actions Have Been Taken. Unable To Pay Back.");
        require(loanObject.borrower == msg.sender, "Access Not Authorized.");
        //Amount to pay 
        uint256 amount_to_pay = loanObject.ether_2_borrow + loanObject.ether_interest_lender + loanObject.ether_interest_guarantor;
        require(amount_to_pay <= msg.value, "Not Enough Funds Sent.");
        _;
    }

    //checks that guarantor is owner of guarantee he is trying to get refund on
    //also loan must not be funded and the date must be overdue
    modifier isGuarantorEligibleRefund(uint256 loanId){
        loan storage loanObject = unfundedLoans[loanId];
        require(loanObject.borrower != 0, "Loan is Already Funded. Cannot Return Refund.");
        require(loanObject.deadline < now, "Loan is still eligible to be funded. Cannot Return Refund.");
        require(loanObject.guarantor == msg.sender, "Access Not Authorized.");
        _;
    }

    //check whether a funder is eligible to fund a loan
    modifier isFunderEligible(uint256 loanId){
        loan storage loanObject = unfundedLoans[loanId];
        require(loanObject.borrower != 0, "Loan is Already Funded. Cannot Fund A Funded Loan.");
        require(loanObject.deadline > now, "Loan cannot be given, since deadline issued by borrower has passed.");
        require(loanObject.ether_2_borrow <= msg.value);
        _;
    }

    //check whether a funder is allowed to get a refund on a loan
    modifier isFunderAllowed(uint256 loanId){
        loan storage loanObject = fundedLoans[loanId];
        require(loanObject.borrower != 0, "Loan is Not Funded. Cannot Refund An Unfunded Loan.");
        require(loanObject.funder ==  msg.sender, "Cannot Refund. Denied Access.");
        require(loanObject.isGuaranteed == true, "Cannot Refund. No Guarantee Detected.");
        require(loanObject.deadline <= now, "Cannot Refund. Dealine Is Not Due Yet.");
        _;
    }

    //--Generic Functions--

    //function to check funds of contract
    function getFunds()public view returns(uint256){
        require(msg.sender == owner, "Unauthorised Access");
        return contractFund;
    }

    //function that gets amount of ether required for Guarantor and Lender
    function getEtherRequired(uint256 loanId) public view returns(uint256){
        require(unfundedLoans[loanId].borrower != 0, "Loan Already Funded. No Need To Enquire About Its Value.");
        //note that the +1 will cover gas costs as well as company services
        return (unfundedLoans[loanId].ether_2_borrow);
    }

    //function that returns total sum of ether to pay - used by borrower to calculate interests
    function getTotalToPay(uint256 loanId) public view returns(uint256){
        require(fundedLoans[loanId].borrower != 0, "Loan Is Not Found In Funded List.");
        uint256 total = fundedLoans[loanId].ether_2_borrow + fundedLoans[loanId].ether_interest_lender + fundedLoans[loanId].ether_interest_guarantor;
        return total;
    }

    //get number of unfunded transactions
    function getTotalUnfundedLoans() public view returns(uint256){
        return currentUnfundedLoans.length;
    }

    //get details of an unfunded loan at particular index
    function getUnfundedLoanDetails(uint256 index) public view returns(uint256, uint256, bool, address, uint256, uint256){
        //if empty list or incorrect index
        if(currentUnfundedLoans.length == 0 || index >=currentUnfundedLoans.length){
            return(0,0,false,0,0,0);
        }
        uint256 loanIndex = currentUnfundedLoans[index];
        loan storage currentLoan = unfundedLoans[loanIndex];
        return(loanIndex, currentLoan.ether_2_borrow, currentLoan.isGuaranteed, currentLoan.guarantor, currentLoan.ether_interest_lender, currentLoan.deadline);
    }

    //get current time in blockchain
    function getCurrentTime() public view returns(uint256){
        return now;
    }

    /* //deletes a specific id from the currentUnfundedLoans Array
    function deleteIdFromArray(uint256 loanId) private{
        uint length = currentUnfundedLoans.length;
        //find index of id
        for (uint i = 0; i < length; i++){
            if (currentUnfundedLoans[i] == loanId){
                break;
            }
        }

        //if loan Id not found do nothing
        //else swap last element in the place of the item you want to delete
        //and delete last element whilst reducing size of array
        if(i==length){
            return;
        }else{
            currentUnfundedLoans[i] = currentUnfundedLoans[length - 1];
            delete currentUnfundedLoans[length - 1];
            currentUnfundedLoans.length--;
        }        
    } */

    //deletes a specific id from a uint256 array
    function deleteLoanIdFromArray(uint256 loanId, uint256[] storage array) private returns(uint256[]){
        uint length = array.length;
        //find index of id
        for (uint i = 0; i < length; i++){
            if (array[i] == loanId){
                break;
            }
        }

        //if loan Id not found do nothing
        //else swap last element in the place of the item you want to delete
        //and delete last element whilst reducing size of array
        if(i==length){
            return array;
        }else{
            array[i] = array[length - 1];
            delete array[length - 1];
            array.length--;
            return array;
        }        
    }

    //Generates a unique id -- uses time block and sender address
    //since time block is the time elapsed from Epoch till request it is suffucient
    //together with the address to generate a unique id.
    //it does not modify the storage -- thus view
    function generateId() private returns(uint256){
        /* address caller = msg.sender;
        uint256 current_time = now;
        //10^10 is the amount of digits time has - it is statically typed since it will take
        //a very long time for it to elapse - improvement could be to write a function
        //that actually checks the length of the number of bytes
        uint256 generated_id =  uint256(caller) * 10000000000;
        generated_id = generated_id + current_time;
        return generated_id; */

        transactionId += 1;
        return transactionId;
    }

    //delete smart contract
    function deleteContract() public{
        require (msg.sender == owner, "Access Denied");
        msg.sender.transfer(contractFund);
        selfdestruct(msg.sender);
    }

    //--Borrower Functions--

    //generates a loan reqeuest and stores it in unfunded list
    //daysToPay is the amount of days that a borrower is saying he will pay loan in eg. will pay in 265 days
    function depositLoanRequest(uint256 etherRequested, uint256 etherInInterest, uint256 daysToPay) public returns(uint256){
        uint256 uniqueId = generateId();
        
        unfundedLoans[uniqueId] = loan({borrower: msg.sender,
                                        funder: 0,
                                        guarantor: 0,
                                        ether_2_borrow: etherRequested,
                                        ether_interest_lender: etherInInterest,
                                        ether_interest_guarantor: 0,
                                        isGuaranteed: false,
                                        //we convert days to seconds and add them to now
                                        deadline: now + (daysToPay*1 days)});
        //add to unfunded loans tracker
        currentUnfundedLoans.push(uniqueId);

        //add loan to borrower
        loansBorrower[msg.sender].push(uniqueId);

        return uniqueId;
    }

    //get number of loans a borrower currently has
    function getNumberLoans() public view returns(uint256){
        return loansBorrower[msg.sender].length;
    }
    
    //get the loan id at supplied index
    function getLoanIdBorrower(uint256 index) public view returns(uint256){
        return loansBorrower[msg.sender][index];
    }

    //get number of guarantees offerred for a particular loan
    function getTotalGuarantees(uint256 loanId) public view returns(uint256){
        return guaranteesProposed[loanId].length;
    }

    //get guarantee details of a particular guarantee id
    function getGuaranteeReceipt(uint256 loanId, uint256 index) public view returns(uint256, address, uint256, uint256){
        if(guaranteesProposed[loanId].length == 0){
            return (0,0,0,0);
        }
        uint256 guaranteeIndex = guaranteesProposed[loanId][index];
        guarantee storage reciept = guaranteeReceipt[guaranteeIndex];
        //if reciept has been deleted return zeros
        if(reciept.borrower == 0){
            return(0,0,0,0);
        }else{
            return(guaranteeIndex,reciept.guarantor ,reciept.guarantor_interest ,reciept.total_interest);
        }
    }

    //accept a particular guarantee whilst deleting all the others
    function acceptGuarantee(uint256 loanId, uint256 guaranteeId) isOwnerOfLoan(loanId) public{

        //check that loan is not funded aka still in unfunded list, since we cannot accept a guarantee for a funded loan
        require(unfundedLoans[loanId].borrower != 0 && unfundedLoans[loanId].funder == 0, "Loan Already Funded. Cannot Accept Guarantee");

        //check that loan does not have a guarantee accepted already
        require(unfundedLoans[loanId].isGuaranteed == false, "Already Has Guarantee");

        //get number of guarantees
        uint256 num_guarantees = getTotalGuarantees(loanId);

        //there must be at least one guarantee for us to accept
        require(num_guarantees > 0, "Cannot Accept Guarantee, Since There Arent Any");


        for(uint256 index = 0 ; index < num_guarantees; index++){
            uint256 currentGuaranteeId = guaranteesProposed[loanId][index];
            address guarantorAd = guaranteeReceipt[currentGuaranteeId].guarantor;
            if(guaranteeId!=currentGuaranteeId){
                uint256 etherDeposited = guaranteeReceipt[currentGuaranteeId].guarantee;
                //delete receipt
                delete guaranteeReceipt[currentGuaranteeId];
                //deduct funds from contract
                contractFund -= etherDeposited;
                //return funds
                guarantorAd.transfer(etherDeposited);
            }else{
                //assign guarantee reciept to not funded loan
                unfundedLoans[loanId].guarantor = guarantorAd;
                unfundedLoans[loanId].ether_interest_lender = unfundedLoans[loanId].ether_interest_lender - guaranteeReceipt[guaranteeId].guarantor_interest;
                unfundedLoans[loanId].ether_interest_guarantor = guaranteeReceipt[guaranteeId].guarantor_interest;
                unfundedLoans[loanId].isGuaranteed = true;

                //add guarantee Id to tracker that tracks which loans a guarantor has guaranteed
                loansGuarantor[guarantorAd].push(loanId);

                //delete receipt
                delete guaranteeReceipt[guaranteeId];
            }
        }

        //delete mapping of loanId to guarantees proposed
        delete guaranteesProposed[loanId];
    }

    //pay loan back
    function payLoanBack(uint256 loanId) isPaybackValid(loanId) public payable{

        //get money in funds 
        contractFund += msg.value;

        uint256 amountPayGuarantor;
        address Guarantor;
        bool isGuaranteed = fundedLoans[loanId].isGuaranteed;

        ///delete loan from list of borrower
        loansBorrower[msg.sender] = deleteLoanIdFromArray(loanId, loansBorrower[msg.sender]);

        if(isGuaranteed == true){
            //delete loan from list of guarantor
            loansGuarantor[fundedLoans[loanId].guarantor] = deleteLoanIdFromArray(loanId, loansGuarantor[fundedLoans[loanId].guarantor]);
            amountPayGuarantor = fundedLoans[loanId].ether_2_borrow + fundedLoans[loanId].ether_interest_guarantor;
            Guarantor = fundedLoans[loanId].guarantor;
            //delete amount to pay guarantor, from fund
            contractFund -= amountPayGuarantor;
        }       

        //delete loan from list of funder
        loansFunder[fundedLoans[loanId].funder] = deleteLoanIdFromArray(loanId, loansFunder[fundedLoans[loanId].funder]);

        uint256 amountPayFunder = fundedLoans[loanId].ether_2_borrow + fundedLoans[loanId].ether_interest_lender;      

        address Funder = fundedLoans[loanId].funder;

        //delete loan from funded list
        delete fundedLoans[loanId];

        //delete amount to pay funder, from fund
        contractFund -= amountPayFunder;        

        //transfer amount
        Funder.transfer(amountPayFunder);

        if(isGuaranteed){
            Guarantor.transfer(amountPayGuarantor);
        }
    }


    //--Guarantor Functions--

    //guarantor deposits guarantee
    function depositGuarantee(uint256 loanId, uint256 etherInInterest) isGuaranteeValid(loanId, etherInInterest) public payable returns(uint256){

        //if no fail add funds to contract
        contractFund+= msg.value;

        loan storage loanObject = unfundedLoans[loanId];

        uint256 uniqueId = generateId();
        
        guaranteeReceipt[uniqueId] = guarantee({  guarantor: msg.sender,
                                                    borrower: loanObject.borrower,
                                                    loanId: loanId,
                                                    guarantee: msg.value,
                                                    total_interest: loanObject.ether_interest_lender,
                                                    guarantor_interest: etherInInterest});

        //add guarantee to guarantees per loan tracker
        guaranteesProposed[loanId].push(uniqueId);

        return uniqueId;
    }

    //get refund on a loan that has accepted your guarantee, however no one accepted the loan 
    function getRefundGuarantor(uint256 loanId) isGuarantorEligibleRefund(loanId) public{
        //deleting mapping between guarantor and loan 
        loansGuarantor[msg.sender] = deleteLoanIdFromArray(loanId, loansGuarantor[msg.sender]);
        
        address borrower = unfundedLoans[loanId].borrower;
        uint256 ether_to_refund = unfundedLoans[loanId].ether_2_borrow;    

        //deleting loan
        delete unfundedLoans[loanId];

        //deleting mapping between borrower and loan
        loansBorrower[borrower] = deleteLoanIdFromArray(loanId, loansBorrower[borrower]);

        //issuing refund to guarantor
        contractFund-= ether_to_refund;
        msg.sender.transfer(ether_to_refund);
    }

    //get number of guarantees a guarantor currently has (only accepted ones)
    function getNumberGuarantees() public view returns(uint256){
        return loansGuarantor[msg.sender].length;
    }
    
    //get the loan id at supplied index
    function getLoanIdGuarantor(uint256 index) public view returns(uint256){
        return loansGuarantor[msg.sender][index];
    }

    //--Functions For Lender--

    //lending money to an unfunded loan
    function lendMoney(uint256 loanId) isFunderEligible(loanId) public payable{
        //save money in fund
        contractFund += msg.value;

        //add loan to funded list
        fundedLoans[loanId] = unfundedLoans[loanId];
        fundedLoans[loanId].funder = msg.sender;

        //remove loan from unfunded loans and from list tracking unfunded loans
        delete unfundedLoans[loanId];
        currentUnfundedLoans = deleteLoanIdFromArray(loanId, currentUnfundedLoans);        

        //add funded address to funder
        loansFunder[msg.sender].push(loanId);

        //send loan to borrower
        contractFund -= fundedLoans[loanId].ether_2_borrow;
        fundedLoans[loanId].borrower.transfer(fundedLoans[loanId].ether_2_borrow);
    }

    //get number of guarantees a guarantor currently has (only accepted ones)
    function getNumberFunds() public view returns(uint256){
        return loansFunder[msg.sender].length;
    }
    
    //get the loan id at supplied index
    function getLoanIdFunder(uint256 index) public view returns(uint256){
        return loansFunder[msg.sender][index];
    }

    //get refund on overdue loan
    function getRefundFunder(uint256 loanId) isFunderAllowed(loanId) public{
        uint256 refund = fundedLoans[loanId].ether_2_borrow;

        //delete loan from list of borrower
        loansBorrower[fundedLoans[loanId].borrower] = deleteLoanIdFromArray(loanId, loansBorrower[fundedLoans[loanId].borrower]);

        //delete loan from list of guarantor
        loansGuarantor[fundedLoans[loanId].guarantor] = deleteLoanIdFromArray(loanId, loansGuarantor[fundedLoans[loanId].guarantor]);

        //delete loan from list of funder
        loansFunder[msg.sender] = deleteLoanIdFromArray(loanId, loansFunder[msg.sender]);

        //delete funded loan from map
        delete fundedLoans[loanId];

        //issue refund to lender
        contractFund -= refund;
        msg.sender.transfer(refund);
    } 
    
}