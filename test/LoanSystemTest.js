var LoanSystem = artifacts.require('./LoanSystem.sol');

contract('LoanSystem', function(accounts){

	let instance;

	beforeEach(async() => {
		instance = await LoanSystem.new()
	})

	it('Should Test: When Borrower deposits loan request, it is added to unfunded loans map and array that keeps track of unfunded loans', async() =>{
		var etherRequested = 5;
		var etherInInterest = 1;
		var daysToPay = 20;

		var numIterations = 3;

		for (var i = 0; i < numIterations; i++) {
			await instance.depositLoanRequest(web3.toWei(etherRequested-i,'ether'),web3.toWei(etherInInterest+i, 'ether'),daysToPay);
		}	
		
		var num_loans = await instance.getTotalUnfundedLoans.call();
		num_loans = num_loans.toNumber();

		assert(numIterations == num_loans, "Not all Loan Requests Where Registered.");

		//check last loan request made details to see that correct data is inputted
		var  loanRequest = await instance.getUnfundedLoanDetails(num_loans-1);
		assert(	loanRequest[1].toNumber() == web3.toWei(etherRequested-numIterations+1,'ether') &&
				loanRequest[2] == false &&
				loanRequest[3] == 0 &&
				loanRequest[4].toNumber() == web3.toWei(etherInInterest+numIterations-1,'ether'), "Loan Request Not Stored appropriately.");
	})

	it('Should Test: When Borrower deposits loan request, it is added to list of loan requests Borrower possesses', async() =>{
		var etherRequested = 5;
		var etherInInterest = 1;
		var daysToPay = 20;

		var numIterations = 5;

		for (var i = 0; i < numIterations; i++) {
			await instance.depositLoanRequest(web3.toWei(etherRequested, 'ether'),web3.toWei(etherInInterest, 'ether'),daysToPay);
		}		
			
		var num_loans = await instance.getNumberLoans.call();
		num_loans = num_loans.toNumber()

		assert(numIterations == num_loans, "I have less contracts assigned then requested.");
	})

	it('Should Test: When Borrower accepts guarantee, loan request status is updated appropriately', async() =>{
		var etherRequested =3;
		var etherInInterest = 2;
		var daysToPay = 20;
		var etherInterestGuarantor = 1;

		//deposit loan request and get its id
		await instance.depositLoanRequest(web3.toWei(etherRequested, 'ether'),web3.toWei(etherInInterest, 'ether'),daysToPay);

		var loanId = await instance.getUnfundedLoanDetails.call(0);
		loanId = loanId[0].toNumber();

		//deposit guarantee for it
		await instance.depositGuarantee(loanId, web3.toWei(etherInterestGuarantor, 'ether'),{value: web3.toWei(etherRequested, 'ether')});	

		//get guarantee id i will accept
		var guaranteeId = await instance.getGuaranteeReceipt.call(loanId, 0);
		guaranteeId = guaranteeId[0].toNumber();

		//accept guarantee
		await instance.acceptGuarantee(loanId, guaranteeId);

		//get loan request
		var loan = await instance.getUnfundedLoanDetails.call(0);

		assert(	loan[2] == true &&
				loan[3] == accounts[0], "Guarantor Address Not Set." &&
				loan[4].toNumber() == web3.toWei(etherInInterest, 'ether') - web3.toWei(etherInterestGuarantor,'ether'), "Loan Request Status not updated appropriately");
	})

	it('Should Test: When Borrower accepts guarantee, guarantee proposals for that loan are deleted.', async() =>{

		var etherRequested =3;
		var etherInInterest = 2;
		var daysToPay = 20;
		var etherInterestGuarantor = 1;

		//deposit loan request and get its id
		await instance.depositLoanRequest(web3.toWei(etherRequested, 'ether'),web3.toWei(etherInInterest, 'ether'),daysToPay);

		var loanId = await instance.getUnfundedLoanDetails.call(0);
		loanId = loanId[0].toNumber();

		//deposit guarantee for it
		await instance.depositGuarantee(loanId, web3.toWei(etherInterestGuarantor, 'ether'),{value: web3.toWei(etherRequested, 'ether')});

		//check it is registered
		var total_guarantees = await instance.getTotalGuarantees.call(loanId);
		assert(total_guarantees > 0, "Guarantee not registered as proposal");	

		//get guarantee id i will accept
		var guaranteeId = await instance.getGuaranteeReceipt.call(loanId, 0);
		guaranteeId = guaranteeId[0].toNumber();

		//accept guarantee
		await instance.acceptGuarantee(loanId, guaranteeId);

		//check guarantee proposals are deleted
		var total_guarantees = await instance.getTotalGuarantees.call(loanId);
		assert(total_guarantees == 0, "Guarantee proposals not deleted");

		//check reciept is also deleted
		var guaranteeId = await instance.getGuaranteeReceipt.call(loanId, 0);
		assert(guaranteeId[0].toNumber() == 0, "Guarantee Reciept not deleted");

	})

	it('Should Test: When Borrower accepts guarantee, guarantor has guarantee added to his list.', async() =>{
		var etherRequested =3;
		var etherInInterest = 2;
		var daysToPay = 20;
		var etherInterestGuarantor = 1;

		//deposit loan request and get its id
		await instance.depositLoanRequest(web3.toWei(etherRequested, 'ether'),web3.toWei(etherInInterest, 'ether'),daysToPay);

		var loanId = await instance.getUnfundedLoanDetails.call(0);
		loanId = loanId[0].toNumber();

		//deposit guarantee for it
		await instance.depositGuarantee(loanId, web3.toWei(etherInterestGuarantor, 'ether'),{value: web3.toWei(etherRequested, 'ether')});

		//get guarantee id i will accept
		var guaranteeId = await instance.getGuaranteeReceipt.call(loanId, 0);
		guaranteeId = guaranteeId[0].toNumber();

		//check guarantee lisst before
		var amntGuaranteesBefore = await instance.getNumberGuarantees.call();

		//accept guarantee
		await instance.acceptGuarantee(loanId, guaranteeId);

		//check guarantee list after
		var amntGuaranteesAfter = await instance.getNumberGuarantees.call();
		assert(amntGuaranteesBefore < amntGuaranteesAfter, "Not Added To List");

	})

	it('Should Test: When Borrower accepts guarantee, guarantors having guarantees rejected get refund.', async() =>{
		var etherRequested =3;
		var etherInInterest = 2;
		var daysToPay = 20;
		var etherInterestGuarantor = 1;

		//deposit loan request and get its id
		await instance.depositLoanRequest(web3.toWei(etherRequested, 'ether'),web3.toWei(etherInInterest, 'ether'),daysToPay);

		var loanId = await instance.getUnfundedLoanDetails.call(0);
		loanId = loanId[0].toNumber();


		//deposit 3 different guarantee for it
		await instance.depositGuarantee(loanId, web3.toWei(etherInterestGuarantor, 'ether'),{from: accounts[1], value: web3.toWei(etherRequested, 'ether')});
		await instance.depositGuarantee(loanId, web3.toWei(etherInterestGuarantor, 'ether'),{from: accounts[2], value: web3.toWei(etherRequested, 'ether')});
		await instance.depositGuarantee(loanId, web3.toWei(etherInterestGuarantor, 'ether'),{from: accounts[3], value: web3.toWei(etherRequested, 'ether')});

		//get guarantee id i will accept
		var guaranteeId = await instance.getGuaranteeReceipt.call(loanId, 0);
		guaranteeId = guaranteeId[0].toNumber();

		//check balances befoer
		var balanceAcc0B = web3.eth.getBalance(accounts[1]);
		var balanceAcc1B = web3.eth.getBalance(accounts[2]);
		var balanceAcc2B = web3.eth.getBalance(accounts[3]);

		//accept guarantee
		await instance.acceptGuarantee(loanId, guaranteeId);

		//check balances befoer
		var balanceAcc0A = web3.eth.getBalance(accounts[1]);
		var balanceAcc1A = web3.eth.getBalance(accounts[2]);
		var balanceAcc2A = web3.eth.getBalance(accounts[3]);
		

		assert(	balanceAcc0A.c.toString() == balanceAcc0B.c.toString() &&
				balanceAcc1A.c.toString() > balanceAcc1B.c.toString() &&
				balanceAcc2A.c.toString() > balanceAcc2B.c.toString(), "Not Properly Refunded.");
	})

	it('Should Test: When Borrower accepts guarantee, he is the actual owner of loan request.', async() =>{
		var etherRequested =3;
		var etherInInterest = 2;
		var daysToPay = 20;
		var etherInterestGuarantor = 1;

		//deposit loan request and get its id
		await instance.depositLoanRequest(web3.toWei(etherRequested, 'ether'),web3.toWei(etherInInterest, 'ether'),daysToPay);

		var loanId = await instance.getUnfundedLoanDetails.call(0);
		loanId = loanId[0].toNumber();


		//deposit guarantee for it
		await instance.depositGuarantee(loanId, web3.toWei(etherInterestGuarantor, 'ether'),{from: accounts[1], value: web3.toWei(etherRequested, 'ether')});

		//get guarantee id i will accept
		var guaranteeId = await instance.getGuaranteeReceipt.call(loanId, 0);
		guaranteeId = guaranteeId[0].toNumber();

		var errorVal = "";

		try{
			//accept guarantee
			await instance.acceptGuarantee(loanId, guaranteeId, {from: accounts[9]});
		}catch(error){
			//we are actually expecting error
			errorVal = error.message;
		}

		assert(errorVal == "VM Exception while processing transaction: revert", "Transaction Went Through With Unauthorized Access");
	})

	it('Should Test: When Borrower tries to accept a guarantee, loan request does not have a guarantor already', async() =>{
		var etherRequested =3;
		var etherInInterest = 2;
		var daysToPay = 20;
		var etherInterestGuarantor = 1;

		//deposit loan request and get its id
		await instance.depositLoanRequest(web3.toWei(etherRequested, 'ether'),web3.toWei(etherInInterest, 'ether'),daysToPay);

		var loanId = await instance.getUnfundedLoanDetails.call(0);
		loanId = loanId[0].toNumber();

		//deposit guarantee for it
		await instance.depositGuarantee(loanId, web3.toWei(etherInterestGuarantor, 'ether'),{from: accounts[1], value: web3.toWei(etherRequested, 'ether')});
		await instance.depositGuarantee(loanId, web3.toWei(etherInterestGuarantor, 'ether'),{from: accounts[2], value: web3.toWei(etherRequested, 'ether')});

		//get guarantee id i will accept
		var guaranteeId = await instance.getGuaranteeReceipt.call(loanId, 0);
		guaranteeId = guaranteeId[0].toNumber();

		//get guarantee id i will try to accept later
		var guaranteeId2 = await instance.getGuaranteeReceipt.call(loanId, 1);
		guaranteeId2 = guaranteeId2[0].toNumber();

		//accept guarantee
		await instance.acceptGuarantee(loanId, guaranteeId);

		var errorVal = "";

		try{
			//accept second guarantee on same loan
			await instance.acceptGuarantee(loanId, guaranteeId2);
		}catch(error){
			//we are actually expecting error
			errorVal = error.message;
		}

		assert(errorVal == "VM Exception while processing transaction: revert", "Second Guarantee Accepted Anyway");

	})

	it('Should Test: When Borrower tries to accept a guarantee, loan request does not have a funder already', async() =>{
		var etherRequested =3;
		var etherInInterest = 2;
		var daysToPay = 20;
		var etherInterestGuarantor = 1;

		//deposit loan request and get its id
		await instance.depositLoanRequest(web3.toWei(etherRequested, 'ether'),web3.toWei(etherInInterest, 'ether'),daysToPay);

		var loanId = await instance.getUnfundedLoanDetails.call(0);
		loanId = loanId[0].toNumber();

		//deposit guarantee for it
		await instance.depositGuarantee(loanId, web3.toWei(etherInterestGuarantor, 'ether'),{from: accounts[1], value: web3.toWei(etherRequested, 'ether')});

		//get guarantee id i will accept
		var guaranteeId = await instance.getGuaranteeReceipt.call(loanId, 0);
		guaranteeId = guaranteeId[0].toNumber();

		//deposit funding on a loan
		await instance.lendMoney(loanId,{value: web3.toWei(etherRequested, 'ether'), from:accounts[9]});

		var errorVal = "";

		try{
			//accept guarantee on funded loan
			await instance.acceptGuarantee(loanId, guaranteeId);
		}catch(error){
			//we are actually expecting error
			errorVal = error.message;
		}

		assert(errorVal == "VM Exception while processing transaction: revert", "Guarantee Accepted Anyway - Even Though There is Funder Already");

	})

	it('Should Test: When Borrower tries to accept a guarantee, loan request must have at least one guarantee proposal', async() =>{
		var etherRequested =3;
		var etherInInterest = 2;
		var daysToPay = 20;
		var etherInterestGuarantor = 1;

		//deposit loan request and get its id
		await instance.depositLoanRequest(web3.toWei(etherRequested, 'ether'),web3.toWei(etherInInterest, 'ether'),daysToPay);

		var loanId = await instance.getUnfundedLoanDetails.call(0);
		loanId = loanId[0].toNumber();

		var errorVal = "";

		try{
			//try to accept guarantee where there are zero existing guarantee proposals
			await instance.acceptGuarantee(loanId, 0);
		}catch(error){
			//we are actually expecting error
			errorVal = error.message;
		}

		assert(errorVal == "VM Exception while processing transaction: revert", "Guarantee Accepted Anyway - Even Though There is Funder Already");
	})

	it('Should Test: When Guarantor deposits Guarantee, guarantee proposal of particular loanRequest increases and guarantee struct is saved in map', async() =>{
		var etherRequested =3;
		var etherInInterest = 2;
		var daysToPay = 20;
		var etherInterestGuarantor = 1;

		//deposit loan request and get its id
		await instance.depositLoanRequest(web3.toWei(etherRequested, 'ether'),web3.toWei(etherInInterest, 'ether'),daysToPay);

		var loanId = await instance.getUnfundedLoanDetails.call(0);
		loanId = loanId[0].toNumber();

		var totalGuaranteesBefore = await instance.getTotalGuarantees.call(loanId);

		//deposit guarantee
		await instance.depositGuarantee(loanId, web3.toWei(etherInterestGuarantor, 'ether'), {from: accounts[3], value: web3.toWei(etherRequested, 'ether')});

		var totalGuaranteesAfter = await instance.getTotalGuarantees.call(loanId);

		assert(totalGuaranteesBefore < totalGuaranteesAfter, "Guarantee not added to proposal list of transaction");

		var indexGuarantee = totalGuaranteesAfter-1;

		var guaranteeReceipt = await instance.getGuaranteeReceipt.call(loanId, indexGuarantee);

		assert(	guaranteeReceipt[1] == accounts[3] &&
				guaranteeReceipt[2].toNumber() == web3.toWei(etherInterestGuarantor, 'ether') &&
				guaranteeReceipt[3].toNumber() == web3.toWei(etherInInterest, 'ether'), "Guarantee Details not properly added to map.");

	})

	it('Should Test: When Guarantor deposits Guarantee, loan request must not be already funded', async() =>{
		var etherRequested =3;
		var etherInInterest = 2;
		var daysToPay = 20;
		var etherInterestGuarantor = 1;

		//deposit loan request and get its id
		await instance.depositLoanRequest(web3.toWei(etherRequested, 'ether'),web3.toWei(etherInInterest, 'ether'),daysToPay);

		var loanId = await instance.getUnfundedLoanDetails.call(0);
		loanId = loanId[0].toNumber();

		//deposit funding
		await instance.lendMoney(loanId, {from: accounts[4], value: web3.toWei(etherRequested,'ether')});

		var errorVal = "";

		try{
			//deposit guarantee
			await instance.depositGuarantee(loanId, web3.toWei(etherInterestGuarantor, 'ether'), {from: accounts[3], value: web3.toWei(etherRequested, 'ether')});
		}catch(error){
			//we are actually expecting error
			errorVal = error.message;
		}

		assert(errorVal == "VM Exception while processing transaction: revert", "Guarantee Accepted Anyway - Even Though There is Funder Already");
		
	})

	it('Should Test: When Guarantor deposits Guarantee, loan request must not already have a guarantee', async() =>{
		var etherRequested =3;
		var etherInInterest = 2;
		var daysToPay = 20;
		var etherInterestGuarantor = 1;

		//deposit loan request and get its id
		await instance.depositLoanRequest(web3.toWei(etherRequested, 'ether'),web3.toWei(etherInInterest, 'ether'),daysToPay);

		var loanId = await instance.getUnfundedLoanDetails.call(0);
		loanId = loanId[0].toNumber();

		//deposit guarantee for it
		await instance.depositGuarantee(loanId, web3.toWei(etherInterestGuarantor, 'ether'),{from: accounts[1], value: web3.toWei(etherRequested, 'ether')});

		//get guarantee id i will accept
		var guaranteeId = await instance.getGuaranteeReceipt.call(loanId, 0);
		guaranteeId = guaranteeId[0].toNumber();

		//accept guarantee
		await instance.acceptGuarantee(loanId, guaranteeId);

		var errorVal = "";

		try{
			//accept second guarantee on same loan
			await instance.depositGuarantee(loanId, web3.toWei(etherInterestGuarantor, 'ether'), {from: accounts[3], value: web3.toWei(etherRequested, 'ether')});
		}catch(error){
			//we are actually expecting error
			errorVal = error.message;
		}

		assert(errorVal == "VM Exception while processing transaction: revert", "Second Guarantee Accepted Anyway");
	})

	it('Should Test: When Guarantor deposits Guarantee, he sends sufficient funds', async() =>{

		var etherRequested =10;
		var etherInInterest = 5;
		var daysToPay = 20;
		var etherInterestGuarantor = 1;

		//deposit loan request and get its id
		await instance.depositLoanRequest(web3.toWei(etherRequested, 'ether'),web3.toWei(etherInInterest, 'ether'),daysToPay);

		var loanId = await instance.getUnfundedLoanDetails.call(0);
		loanId = loanId[0].toNumber();

		var errorVal = "";

		try{
			//deposit guarantee with less than sufficien funds
			await instance.depositGuarantee(loanId, web3.toWei(etherInterestGuarantor, 'ether'),{from: accounts[1], value: web3.toWei(etherRequested-1, 'ether')});
		}catch(error){
			//we are actually expecting error
			errorVal = error.message;
		}

		assert(errorVal == "VM Exception while processing transaction: revert", "Second Guarantee Accepted Anyway");		
	})

	it('Should Test: When Guarantor deposits Guarantee, the requested interest is not greater than the maximum offerred by borrower.', async() =>{
		var etherRequested =10;
		var etherInInterest = 5;
		var daysToPay = 20;
		var etherInterestGuarantor = 1;

		//deposit loan request and get its id
		await instance.depositLoanRequest(web3.toWei(etherRequested, 'ether'),web3.toWei(etherInInterest, 'ether'),daysToPay);

		var loanId = await instance.getUnfundedLoanDetails.call(0);
		loanId = loanId[0].toNumber();

		var errorVal = "";

		try{
			//deposit guarantee with interest higher
			await instance.depositGuarantee(loanId, web3.toWei(etherInInterest+1, 'ether'),{from: accounts[1], value: web3.toWei(etherRequested, 'ether')});
		}catch(error){
			//we are actually expecting error
			errorVal = error.message;
		}

		assert(errorVal == "VM Exception while processing transaction: revert", "Second Guarantee Accepted Anyway");	
	})

	it('Should Test: When Guarantor deposits Guarantee, loan request must not be already overdue', async() =>{

		var etherRequested =10;
		var etherInInterest = 5;
		var daysToPay = 20;
		var etherInterestGuarantor = 1;

		//deposit loan request and get its id
		await instance.depositLoanRequest(web3.toWei(etherRequested, 'ether'),web3.toWei(etherInInterest, 'ether'),(daysToPay*-1));

		var loanId = await instance.getUnfundedLoanDetails.call(0);
		loanId = loanId[0].toNumber();

		var errorVal = "";

		try{
			//deposit guarantee with less than sufficien funds
			await instance.depositGuarantee(loanId, web3.toWei(etherInInterest, 'ether'),{from: accounts[1], value: web3.toWei(etherRequested, 'ether')});
		}catch(error){
			//we are actually expecting error
			errorVal = error.message;
		}

		assert(errorVal == "VM Exception while processing transaction: revert", "Second Guarantee Accepted Anyway");	
		
	})

	it('Should Test: When Guarantor deposits Guarantee, money is saved in contract fund', async() =>{

		var etherRequested =5;
		var etherInInterest = 2;
		var daysToPay = 20;
		var etherInterestGuarantor = 1;

		//deposit loan request and get its id
		await instance.depositLoanRequest(etherRequested,etherInInterest,(daysToPay), {from: accounts[8]});

		var loanId = await instance.getUnfundedLoanDetails.call(0);

		loanId = loanId[0].toNumber();

		var contractFundB = await instance.getFunds.call();

		await instance.depositGuarantee(loanId, etherInterestGuarantor,{from: accounts[7], value: etherRequested});
		
		var contractFundA = await instance.getFunds.call();

		assert(contractFundA.toNumber() == contractFundB.toNumber() + etherRequested, "Funds Not Saved");

	})

	it('Should Test: When Lender deposits Money, enough funds are sent', async() =>{

		var etherRequested =3;
		var etherInInterest = 2;
		var daysToPay = 20;
		var etherInterestGuarantor = 1;

		//deposit loan request and get its id
		await instance.depositLoanRequest(etherRequested,etherInInterest,daysToPay);

		var loanId = await instance.getUnfundedLoanDetails.call(0);
		loanId = loanId[0].toNumber();

		var errorVal = "";

		try{
			//deposit loan with less than sufficien funds
			await instance.lendMoney(loanId,{from: accounts[1], value: etherRequested-1});
		}catch(error){
			//we are actually expecting error
			errorVal = error.message;
		}

		assert(errorVal == "VM Exception while processing transaction: revert", "Money Accepted Anyway");		

	})

	it('Should Test: When Lender deposits Money, money is sent to borrower', async() =>{
		var etherRequested =3;
		var etherInInterest = 2;
		var daysToPay = 20;
		var etherInterestGuarantor = 1;

		//deposit loan request and get its id
		await instance.depositLoanRequest(etherRequested,etherInInterest,daysToPay, {from: accounts[3]});

		var loanId = await instance.getUnfundedLoanDetails.call(0);
		loanId = loanId[0].toNumber();

		var before = web3.eth.getBalance(accounts[3]);
		before = before.toString();

		await instance.lendMoney(loanId,{from: accounts[1], value: etherRequested});

		var after = web3.eth.getBalance(accounts[3]);
		after = after.toString();

		assert(after > before, "Money Not Transferred");
	})

	it('Should Test: When Lender deposits Money, loan request is removed from unfunded', async() =>{

		var etherRequested =3;
		var etherInInterest = 2;
		var daysToPay = 20;
		var etherInterestGuarantor = 1;

		//deposit loan request and get its id
		await instance.depositLoanRequest(etherRequested,etherInInterest,daysToPay, {from: accounts[3]});

		var loanId = await instance.getUnfundedLoanDetails.call(0);
		loanId = loanId[0].toNumber();

		var before = await instance.getTotalUnfundedLoans.call();
		before = before.toNumber();

		await instance.lendMoney(loanId,{from: accounts[4], value: etherRequested});

		var after = await instance.getTotalUnfundedLoans.call();
		after = after.toNumber();

		assert(before > after, "Not Deleted");

		var loanDetails = await instance.getUnfundedLoanDetails.call(before-1);

		assert(loanDetails[0] == 0, "Not Deleted");
		
	})

	it('Should Test: When Lender deposits Money, fund be added to funder list', async() =>{
		var etherRequested =3;
		var etherInInterest = 2;
		var daysToPay = 20;
		var etherInterestGuarantor = 1;

		//deposit loan request and get its id
		await instance.depositLoanRequest(etherRequested,etherInInterest,daysToPay, {from: accounts[3]});

		var loanId = await instance.getUnfundedLoanDetails.call(0);
		loanId = loanId[0].toNumber();

		var before = await instance.getNumberFunds.call({from: accounts[4]});
		before = before.toNumber();

		await instance.lendMoney(loanId,{from: accounts[4], value: etherRequested});

		var after = await instance.getNumberFunds.call({from: accounts[4]});
		after = after.toNumber();

		assert(before < after, "Not Added");
	})

	it('Should Test: When Lender tries to deposit Money, he gets stopped if loan request is already overdue', async() =>{

		var etherRequested =3;
		var etherInInterest = 2;
		var daysToPay = 20;
		var etherInterestGuarantor = 1;

		//deposit loan request and get its id
		await instance.depositLoanRequest(web3.toWei(etherRequested,'ether'),web3.toWei(etherInInterest,'ether'),(daysToPay*-1), {from: accounts[6]});

		var loanId = await instance.getUnfundedLoanDetails.call(0);
		loanId = loanId[0].toNumber();

		var before = web3.eth.getBalance(accounts[6]);
		before = before.toString();

		//since function uses a large amount of gas we cannot check for sure if refund is issued with means we have
		var errorMess = "";

		try{
			await instance.lendMoney(loanId,{from: accounts[3], value: web3.toWei(etherRequested,'ether')});
		}catch(error){
			errorMess = error.message;
		}

		var after = web3.eth.getBalance(accounts[6]);
		after = after.toString();

		assert(	errorMess == "VM Exception while processing transaction: revert" &&
				before == after, "Transaction Went Through.");
		
	})

	it('Should Test: When Lender tries to deposits Money, he gets stopped if loan request is already funded', async() =>{
		var etherRequested =3;
		var etherInInterest = 2;
		var daysToPay = 20;
		var etherInterestGuarantor = 1;

		//deposit loan request and get its id
		await instance.depositLoanRequest(web3.toWei(etherRequested,'ether'),web3.toWei(etherInInterest,'ether'),(daysToPay), {from: accounts[6]});

		var loanId = await instance.getUnfundedLoanDetails.call(0);
		loanId = loanId[0].toNumber();

		await instance.lendMoney(loanId,{from: accounts[8], value: web3.toWei(etherRequested,'ether')});

		var before = web3.eth.getBalance(accounts[6]);
		before = before.toString();

		var errorMess = "";

		try{
			await instance.lendMoney(loanId,{from: accounts[3], value: web3.toWei(etherRequested,'ether')});
		}catch(error){
			errorMess = error.message;
		}

		var after = web3.eth.getBalance(accounts[6]);
		after = after.toString();

		assert(	errorMess == "VM Exception while processing transaction: revert" &&
				before == after, "Transaction Went Through.");
	})

	it('Should Test: When Borrower tries to pay back loan, if amount of funds he sends arent enough to cover loan and interest, he is stopped', async() =>{
		var etherRequested =3;
		var etherInInterest = 2;
		var daysToPay = 20;
		var etherInterestGuarantor = 1;

		//deposit loan request and get its id
		await instance.depositLoanRequest(web3.toWei(etherRequested,'ether'),web3.toWei(etherInInterest,'ether'),(daysToPay), {from: accounts[6]});

		var loanId = await instance.getUnfundedLoanDetails.call(0);
		loanId = loanId[0].toNumber();

		//funding loan
		await instance.lendMoney(loanId,{from: accounts[8], value: web3.toWei(etherRequested,'ether')});

		var errorMess = "";

		var totalToPay = await instance.getTotalToPay.call(loanId);

		totalToPay -=web3.toWei(etherInInterest,'ether'); 

		try{
			await instance.payLoanBack(loanId, {from: accounts[6], value : totalToPay - 2});
		}catch(error){
			errorMess = error.message;
		}

		assert(	errorMess == "VM Exception while processing transaction: revert", "Transaction Went Through.");
	})


	//cannot simulate since we cannot lend money to an overdue loan.
	/*it('Should Test: When Lender tries to pay back loan, if loan is already overdue, he is stopped', async() =>{
		var etherRequested =3;
		var etherInInterest = 2;
		var daysToPay = 20;
		var etherInterestGuarantor = 1;

		//deposit loan request and get its id
		await instance.depositLoanRequest(web3.toWei(etherRequested,'ether'),web3.toWei(etherInInterest,'ether'),(daysToPay*-1), {from: accounts[6]});

		var loanId = await instance.getUnfundedLoanDetails.call(0);
		loanId = loanId[0].toNumber();

		//funding loan
		await instance.lendMoney(loanId,{from: accounts[8], value: web3.toWei(etherRequested,'ether')});

		var errorMess = "";

		var totalToPay = await instance.getTotalToPay.call(loanId);

		try{
			await instance.payLoanBack(loanId, {from: accounts[6], value : totalToPay});
		}catch(error){
			errorMess = error.message;
		}

		assert(	errorMess == "VM Exception while processing transaction: revert", "Transaction Went Through.");
		
	})*/

	it('Should Test: When Borrower tries to pay back loan, but loan is not his, he is stopped', async() =>{
		var etherRequested =3;
		var etherInInterest = 2;
		var daysToPay = 20;
		var etherInterestGuarantor = 1;

		//deposit loan request and get its id
		await instance.depositLoanRequest(web3.toWei(etherRequested,'ether'),web3.toWei(etherInInterest,'ether'),(daysToPay), {from: accounts[6]});

		var loanId = await instance.getUnfundedLoanDetails.call(0);
		loanId = loanId[0].toNumber();

		//funding loan
		await instance.lendMoney(loanId,{from: accounts[8], value: web3.toWei(etherRequested,'ether')});

		var errorMess = "";

		var totalToPay = await instance.getTotalToPay.call(loanId);

		try{
			await instance.payLoanBack(loanId, {from: accounts[4], value :totalToPay});
		}catch(error){
			errorMess = error.message;
		}

		assert(	errorMess == "VM Exception while processing transaction: revert", "Transaction Went Through.");		
	})

	it('Should Test: When Borrower tries to pay back loan, all parties involved are paid appropriately', async() =>{
		var etherRequested =3;
		var etherInInterest = 2;
		var daysToPay = 20;
		var etherInterestGuarantor = 1;

		//deposit loan request and get its id
		await instance.depositLoanRequest(web3.toWei(etherRequested,'ether'),web3.toWei(etherInInterest,'ether'),(daysToPay), {from: accounts[6]});

		var loanId = await instance.getUnfundedLoanDetails.call(0);
		loanId = loanId[0].toNumber();

		//depositing guarantee on loan 
		await instance.depositGuarantee(loanId, web3.toWei(etherInterestGuarantor, 'ether'),{from: accounts[8], value: web3.toWei(etherRequested, 'ether')});

		//get guarantee id i will accept
		var guaranteeId = await instance.getGuaranteeReceipt.call(loanId, 0);
		guaranteeId = guaranteeId[0].toNumber();

		//accept guarantee
		await instance.acceptGuarantee(loanId, guaranteeId, {from: accounts[6]});

		//funding loan
		await instance.lendMoney(loanId,{from: accounts[4], value: web3.toWei(etherRequested,'ether')});

		var totalToPay = await instance.getTotalToPay.call(loanId);

		//get balances before
		var before1 = web3.eth.getBalance(accounts[8]);
		before1 = web3.toBigNumber(before1);

		var before2 = web3.eth.getBalance(accounts[4]);
		before2 = web3.toBigNumber(before2);

		await instance.payLoanBack(loanId, {from: accounts[6], value :totalToPay});

		//get balances before
		var after1 = web3.eth.getBalance(accounts[8]);
		after1 = web3.toBigNumber(after1);
		after1 = after1.toString();

		var after2 = web3.eth.getBalance(accounts[4]);
		after2 = web3.toBigNumber(after2);
		after2 = after2.toString();

		var var1 = etherRequested+etherInterestGuarantor;
		var1 = web3.toWei(var1);

		var var2 = etherRequested+(etherInInterest - etherInterestGuarantor);
		var2 = web3.toWei(var2);

		before1 = (before1.plus(web3.toBigNumber(var1))).toString();
		before2 = (before2.plus(web3.toBigNumber(var2))).toString();

		assert(after1 == before1 && after2 == before2, "Not All Parties Were Properly Paid");		
	})

	it('Should Test: When Borrower tries to pay back loan, loan gets deleted from record of participants', async() =>{
		var etherRequested =3;
		var etherInInterest = 2;
		var daysToPay = 20;
		var etherInterestGuarantor = 1;

		//deposit loan request and get its id
		await instance.depositLoanRequest(web3.toWei(etherRequested,'ether'),web3.toWei(etherInInterest,'ether'),(daysToPay), {from: accounts[6]});

		var loanId = await instance.getUnfundedLoanDetails.call(0);
		loanId = loanId[0].toNumber();

		//depositing guarantee on loan 
		await instance.depositGuarantee(loanId, web3.toWei(etherInterestGuarantor, 'ether'),{from: accounts[8], value: web3.toWei(etherRequested, 'ether')});

		//get guarantee id i will accept
		var guaranteeId = await instance.getGuaranteeReceipt.call(loanId, 0);
		guaranteeId = guaranteeId[0].toNumber();

		//accept guarantee
		await instance.acceptGuarantee(loanId, guaranteeId, {from: accounts[6]});

		//funding loan
		await instance.lendMoney(loanId,{from: accounts[4], value: web3.toWei(etherRequested,'ether')});

		var totalToPay = await instance.getTotalToPay.call(loanId);

		//get before
		var before1 = await instance.getNumberFunds.call({from: accounts[4]});
		before1 = before1.toNumber();
		var before2 = await instance.getNumberGuarantees.call({from: accounts[8]});
		before2 = before2.toNumber();
		var before3 = await instance.getNumberLoans.call({from: accounts[6]});
		before3 = before3.toNumber();

		await instance.payLoanBack(loanId, {from: accounts[6], value :totalToPay});

		//get after
		var after1 = await instance.getNumberFunds.call({from: accounts[4]});
		after1 = after1.toNumber();
		var after2 = await instance.getNumberGuarantees.call({from: accounts[8]});
		after2 = after2.toNumber();
		var after3 = await instance.getNumberLoans.call({from: accounts[6]});
		after3 = after3.toNumber();

		assert(after1 < before1 && after2 < before2 && after3 < before3, "Not All Parties Were Properly Cleaned");	
	})				
})

