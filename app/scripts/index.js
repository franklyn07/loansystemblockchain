// Import the page's CSS. Webpack will know what to do with it.
import "../styles/app.css";

// Import libraries we need.
import { default as Web3} from 'web3';
import { default as contract } from 'truffle-contract'

/*
 * When you compile and deploy your Voting contract,
 * truffle stores the abi and deployed address in a json
 * file in the build directory. We will use this information
 * to setup a Voting abstraction. We will use this abstraction
 * later to create an instance of the Voting contract.*/

 import systemArtifact from '../../build/contracts/LoanSystem.json'

 var LoanSystem = contract(systemArtifact);

 // first functions to get called, since they establish the web3 connection and make sure browser is web3 compatible
$( document ).ready(function() {
  if (typeof web3 !== 'undefined') {
    console.warn("Using web3 detected from external source like Metamask")
    // Use Mist/MetaMask's provider
    window.web3 = new Web3(web3.currentProvider);
  } else {
    console.warn("No web3 detected. Falling back to http://localhost:8545. You should remove this fallback when you deploy live, as it's inherently insecure. Consider switching to Metamask for development. More info here: http://truffleframework.com/tutorials/truffle-and-metamask");
    // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
    window.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
  }

  LoanSystem.setProvider(web3.currentProvider);  
});

window.requestLoan = function(){
	//get the values in the candidate and vote-tokens id's and store them
  let etherRequested = $("#etherLoanBorrower").val();
  let deadlineDays = $("#noOfDaysBorrower").val();
  let etherInterest = $("#etherInterestBorrower").val();

  if(etherRequested <= 0 || deadlineDays <=0 || etherInterest <= 0){
  	$("#responseRequestLoan").html("Non negative values are required.");
  	return;
  }

  etherRequested = web3.toWei(etherRequested,'ether');
  etherInterest = web3.toWei(etherInterest, 'ether');

  //alert user with loan request submission
  $("#responseRequestLoan").html("Loan request has been submitted. Your loan list will be submitted as soon as the transaction is recorded on the blockchain. Please wait.");
  //empty input boxes after submission
  $("#etherLoanBorrower").val("");
  $("#noOfDaysBorrower").val("");
  $("#etherInterestBorrower").val("");

  /* LoanSystem.deployed() returns an instance of the contract. Every call
   * in Truffle returns a promise which is why we have used then()
   * everywhere we have a transaction call
   */
  LoanSystem.deployed().then(function(contractInstance) {
  //call the contract function , depositLoanRequest()
  //note all transactions are being performed by user 0 by default
  //this function spends gas, since it changes state of blockchain
    contractInstance.depositLoanRequest(etherRequested, etherInterest, deadlineDays, {from: web3.eth.accounts[0]});
  	//note right now it is not possible to get values returned by functions that modify the blockchain
  	//form off the chain callers (aka only other smart contracts can get these values)
  });
}

window.payLoan = function(){
	//get the values in the candidate and vote-tokens id's and store them
  let loanId = $("#loanIdPayLoan").val();

  if(loanId <= 0){
  	$("#responsePayLoan").html("Invalid Id.");
  	return;
  }

  LoanSystem.deployed().then(function(contractInstance) {
  	contractInstance.getTotalToPay.call(loanId, {from: web3.eth.accounts[0]}).then(function(total){
  		let totalToPay = total;
  		totalToPay = web3.toBigNumber(totalToPay);

  		contractInstance.payLoanBack(loanId, {from: web3.eth.accounts[0], value: totalToPay}); 
  	});
  });


  //alert user with loan request submission
  $("#responsePayLoan").html("Loan payment submitted. Your loan list will be updated as soon as the transaction is recorded on the blockchain. Please wait.");
  //empty input boxes after submission
  $("#loanIdPayLoan").val("");
}

window.acceptGuarantee = function(){
	//get the values in the candidate and vote-tokens id's and store them
  let loanId = $("#loanIdAcceptGuarantee").val();
  let guaranteeId = $("#guaranteeIdAcceptGuarantee").val();

  if(loanId <= 0 || guaranteeId <=0){
  	$("#msgAcceptGuarantee").html("Invalid Id's.");
  	return;
  }

  LoanSystem.deployed().then(function(contractInstance) {
  	contractInstance.acceptGuarantee(loanId, guaranteeId, {from: web3.eth.accounts[0]}).then(function(){
  	//alert user with loan request submission
  	$("#msgAcceptGuarantee").html("Guarantee acceptance submitted. Your loan list will be updated as soon as the transaction is recorded on the blockchain. Please wait.");
	  //empty input boxes after submission
	  $("#loanIdAcceptGuarantee").val("");
	  $("#guaranteeIdAcceptGuarantee").val("");
  	});
  });
}

window.getGuarantees = function(){
	let loanId = $("#loanIdLoanGuarantees").val();

  if(loanId <= 0){
  	$("#msgGetGuarantee").html("Invalid Id's.");
  	return;
  }

	LoanSystem.deployed().then(function(contractInstance) {

	    contractInstance.getTotalGuarantees.call(loanId, {from: web3.eth.accounts[0]}).then(function(totalGuarantees){
		    if(totalGuarantees > 0){	
		    	var currentRecord;
		      for(let i=0; i < totalGuarantees; i++) {
		        contractInstance.getGuaranteeReceipt.call(loanId,i, {from: web3.eth.accounts[0]}).then(function(receipt){
		        	currentRecord = receipt;
		        

		        let guaranteeIndex = currentRecord[0];
		        let guarantorAddress = currentRecord[1];
		        let guarantorInterest = currentRecord[2];
		        let totalInterest = currentRecord[3];

		        $("#loanGuaranteesRow").append("<tr><td>" + guaranteeIndex.toString() + "</td><td>" + guarantorAddress.toString() + "</td><td>" + guarantorInterest.toString() + "</td><td>" + totalInterest.toString() + "</td></tr>");
						});
		      }
		    }else{
		    	$("#msgGetGuarantee").html("No Guarantees For Supplied Loan Id.");
		    }
	    });
	  });
}

window.clearGuaranteesTable = function(){
	var element = document.getElementById("loanGuaranteesTable");
	element.removeChild(element.getElementsByTagName("tbody")[0]);

	var p = document.getElementById("loanGuaranteesTable");
	var newElement = document.createElement("TBODY");
	newElement.setAttribute('id',"loanGuaranteesRow");
	p.appendChild(newElement);
}

window.getMyLoans = function(){

	LoanSystem.deployed().then(function(contractInstance) {

	    contractInstance.getNumberLoans.call({from: web3.eth.accounts[0]}).then(function(totalLoans){
		    if(totalLoans>0){	
		      for(let i=0; i < totalLoans; i++) {
		        contractInstance.getLoanIdBorrower.call(i,{from: web3.eth.accounts[0]}).then(function(loanId){
		        	$("#loanMyLoansRow").append("<tr><td>" + loanId.toString() + "</td></tr>");
		        });
		      }
		    }else{
		    	$("#msgGetMyLoans").html("You Currently Have No Loans.");
		    }
	    });
	  });
}

window.clearMyLoansTable = function(){
	var element = document.getElementById("myLoansTable");
	element.removeChild(element.getElementsByTagName("tbody")[0]);

	var p = document.getElementById("myLoansTable");
	var newElement = document.createElement("TBODY");
	newElement.setAttribute('id',"loanMyLoansRow");
	p.appendChild(newElement);
}


window.depositGuarantee =function(){
	//get the values in the candidate and vote-tokens id's and store them
  let loanId = $("#loanIdDepositGuarantee").val();
  let etherInterest = $("#interestDepositGuarantee").val();

  if(loanId <= 0 || etherInterest <= 0){
  	$("#msgDepositGurantee").html("Non negative values are required.");
  	return;
  }

  etherInterest = web3.toWei(etherInterest, 'ether');

  //alert user with loan request submission
  $("#msgDepositGurantee").html("Guarantee has been submitted.");
  //empty input boxes after submission
  $("#loanIdDepositGuarantee").val("");
  $("#interestDepositGuarantee").val("");


  LoanSystem.deployed().then(function(contractInstance) {
  	contractInstance.getEtherRequired.call(loanId,{from: web3.eth.accounts[0]}).then(function(totalEther){
  		contractInstance.depositGuarantee(loanId, etherInterest, {from: web3.eth.accounts[0], value: totalEther});
  	});
  });
}

window.getLoansGuaranteed = function(){
	LoanSystem.deployed().then(function(contractInstance) {
	    contractInstance.getNumberGuarantees.call({from: web3.eth.accounts[0]}).then(function(totalLoans){
		    if(totalLoans>0){	
		      for(let i=0; i < totalLoans; i++) {
		        contractInstance.getLoanIdGuarantor.call(i, {from: web3.eth.accounts[0]}).then(function(loanId){
		        	$("#loansIGuaranteedRow").append("<tr><td>" + loanId.toString() + "</td></tr>");
		        });
		      }
		    }else{
		    	$("#responseMyGuarantees").html("You Currently Have No Guarantees Accepted.");
		    }
	    });
	  });
}

window.clearLoansGuaranteed = function(){
	var element = document.getElementById("tableLoansIGuaranteed");
	element.removeChild(element.getElementsByTagName("tbody")[0]);

	var p = document.getElementById("tableLoansIGuaranteed");
	var newElement = document.createElement("TBODY");
	newElement.setAttribute('id',"loansIGuaranteedRow");
	p.appendChild(newElement);
}


window.getUnfundedLoansGuarantor = function(){
	LoanSystem.deployed().then(function(contractInstance) {
	    contractInstance.getTotalUnfundedLoans.call({from: web3.eth.accounts[0]}).then(function(totalLoans){
		    if(totalLoans>0){	
		    	var currentRecord;
		      for(let i=0; i < totalLoans; i++) {
		        contractInstance.getUnfundedLoanDetails.call(i, {from: web3.eth.accounts[0]}).then(function(loanRecord){
		        	currentRecord = loanRecord;

							let loanId = currentRecord[0];
							let isGuranteed = currentRecord[2];
							let etherToBorrow = currentRecord[1];
							let guarantor = currentRecord[3];
							let etherInterest = currentRecord[4];
							let deadline = currentRecord[5].toNumber();
							var now;

							contractInstance.getCurrentTime.call({from: web3.eth.accounts[0]}).then(function(nowVal){
			        	now = nowVal.toNumber();
			        
								let remaining_seconds = deadline - now;

								var remaining_days_message = getRemainingTime(remaining_seconds);

			        	$("#unfundedLoansRow").append("<tr><td>" + loanId.toString() + "</td><td>" + isGuranteed.toString() + "</td><td>" + guarantor.toString() + "</td><td>" + etherToBorrow.toString() + "</td><td>" + etherInterest.toString() + "</td><td>" + remaining_days_message + "</td></tr>");
			      	});
			      });	
			     }

		    }else{
		    	$("#responseUnfundedLoans").html("There are currently no unfunded loans.");
		    }
	    });
	  });
}

window.getUnfundedLoansLender = function(){
	LoanSystem.deployed().then(function(contractInstance) {
    contractInstance.getTotalUnfundedLoans.call({from: web3.eth.accounts[0]}).then(function(totalLoans){
	    if(totalLoans>0){	
	    	var currentRecord;
	      for(let i=0; i < totalLoans; i++) {
	        contractInstance.getUnfundedLoanDetails.call(i, {from: web3.eth.accounts[0]}).then(function(loanRecord){
	        	currentRecord = loanRecord;

						let loanId = currentRecord[0];
						let isGuranteed = currentRecord[2];
						let etherToBorrow = currentRecord[1];
						let guarantor = currentRecord[3];
						let etherInterest = currentRecord[4];
						let deadline = currentRecord[5].toNumber();
						
						contractInstance.getCurrentTime.call({from: web3.eth.accounts[0]}).then(function(nowVal){
		        	var now = nowVal.toNumber();		        

							let remaining_seconds = deadline - now;

							var remaining_days_message = getRemainingTime(remaining_seconds);

			        $("#unfundedLoansRow").append("<tr><td>" + loanId.toString() + "</td><td>" + isGuranteed.toString() + "</td><td>" + guarantor.toString() + "</td><td>" + etherToBorrow.toString() + "</td><td>" + etherInterest.toString() + "</td><td>" + remaining_days_message + "</td></tr>");
		      		});
		      	});
	      }
	    }else{
	    	$("#responseUnfundedLoans").html("There are currently no unfunded loans.");
	    }
    });
  });
}

window.clearUnfundedLoans = function(){
	var element = document.getElementById("unfundedLoansTable");
	element.removeChild(element.getElementsByTagName("tbody")[0]);

	var p = document.getElementById("unfundedLoansTable");
	var newElement = document.createElement("TBODY");
	newElement.setAttribute('id',"unfundedLoansRow");
	p.appendChild(newElement);
}

function getRemainingTime(delta){
	var days_mess = "";

	// calculate (and subtract) whole days
	var days = Math.floor(delta / 86400);
	delta -= days * 86400;

	days_mess+=(days.toString() + "Days, ");

	// calculate (and subtract) whole hours
	var hours = Math.floor(delta / 3600) % 24;
	delta -= hours * 3600;

	days_mess+=(hours.toString() + "Hours, ");

	// calculate (and subtract) whole minutes
	var minutes = Math.floor(delta / 60) % 60;
	delta -= minutes * 60;

	days_mess+=(minutes.toString() + "Minutes, ");

	// what's left is seconds
	var seconds = delta % 60;

	days_mess+=(seconds.toString() + "Seconds.");

	return days_mess;
}

window.lendMoney = function(){
//get the values in the candidate and vote-tokens id's and store them
  let loanId = $("#loanIdLendMoney").val();

  if(loanId <= 0){
  	$("#responseLendMoney").html("Non negative values are required.");
  	return;
  }

  LoanSystem.deployed().then(function(contractInstance) {
  	contractInstance.getEtherRequired.call(loanId, {from: web3.eth.accounts[0]}).then(function(totalEther){
  		contractInstance.lendMoney(loanId, {from: web3.eth.accounts[0], value: totalEther});
  	});
  });

  //alert user with loan request submission
  $("#responseLendMoney").html("Loan has been submitted.");
  //empty input boxes after submission
  $("#loanIdLendMoney").val("");
}

window.getLoansFunded = function(){
	LoanSystem.deployed().then(function(contractInstance) {
	    contractInstance.getNumberFunds.call({from: web3.eth.accounts[0]}).then(function(totalLoans){
		    if(totalLoans>0){	
		      for(let i=0; i < totalLoans; i++) {
		        contractInstance.getLoanIdFunder.call(i, {from: web3.eth.accounts[0]}).then(function(loanId){
		        	$("#loansIFundedRow").append("<tr><td>" + loanId.toString() + "</td></tr>");
		        });
		      }
		    }else{
		    	$("#responseLoansIFunded").html("You Currently Have Funded No Loans.");
		    }
	    });
	  });
}

window.clearLoansFunded = function(){
	var element = document.getElementById("tableLoansIFunded");
	element.removeChild(element.getElementsByTagName("tbody")[0]);

	var p = document.getElementById("tableLoansIFunded");
	var newElement = document.createElement("TBODY");
	newElement.setAttribute('id',"loansIFundedRow");
	p.appendChild(newElement);
}

window.getRefundGuarantee = function(){
	let loanId = $("#loanIdLendMoney").val();

  if(loanId <= 0){
  	$("#msgRefundGuarantor").html("Non negative values are required.");
  	return;
  }

  LoanSystem.deployed().then(function(contractInstance) {
  	contractInstance.getRefundGuarantor(loanId, {from: web3.eth.accounts[0]});
  	$("#msgRefundGuarantor").html("Refund Request Submitted.");
  });
}

window.getRefundLoan = function(){
	let loanId = $("#loanIdRefundFunder").val();

  if(loanId <= 0){
  	$("#msgRefundFunder").html("Non negative values are required.");
  	return;
  }

  LoanSystem.deployed().then(function(contractInstance) {
  	contractInstance.getRefundFunder(loanId, {from: web3.eth.accounts[0]});
  	$("#msgRefundFunder").html("Refund Request Submitted.");
  });
}