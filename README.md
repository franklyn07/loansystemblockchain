This project consists of a loan system application that makes use of the 
ethereum blockchain as its backend, exploiting the benefits that this kind of
technology can propose to this kind of application, such as transparency of
transactions and immutability.

The folders contained are the following:

## Report
Contains documentation material such as code snippets, application and
tx receipt snippets, tex files etc. These are all used to produce the final
documentation which can be found in **Documentation.pdf**, within the same 
folder. 

## App
Contains front end related files such as HTML files and their corresponding
javascript files.

## Build
Contains build files produced by the truffle framework.

## Contracts
Contains the smart contract file which acts as a back end to the whole system,
living on the thereum blockchain. Also contains the migration file, used for 
migration purposes upon the blockchain as well.

## Migration
Contains migration files.

## Node Modules
Contains npm module files

## SRC
Contains libraries (eg. bootstrap, css, web3.min.js) etc.

## Test
Contains the test file used to toroughly test the smart contract for bugs.

## Rest
The rest of the files are files which are not of direct importance to the project