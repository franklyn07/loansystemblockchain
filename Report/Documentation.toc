\contentsline {section}{\numberline {1}Introduction}{4}{section.1}
\contentsline {section}{\numberline {2}Contract Structural Design}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}Data Structures Used}{4}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}constructor()}{5}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}struct loan}{5}{subsubsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.3}struct guarantee}{6}{subsubsection.2.1.3}
\contentsline {subsubsection}{\numberline {2.1.4}uint256 [] private currentUnfundedLoans}{6}{subsubsection.2.1.4}
\contentsline {subsubsection}{\numberline {2.1.5}mapping(uint256 $=>$ loan) private unfundedLoans}{6}{subsubsection.2.1.5}
\contentsline {subsubsection}{\numberline {2.1.6}mapping(uint256 $=>$ loan) private fundedLoans}{6}{subsubsection.2.1.6}
\contentsline {subsubsection}{\numberline {2.1.7}mapping(address $=>$ uint256[]) private loansBorrower}{7}{subsubsection.2.1.7}
\contentsline {subsubsection}{\numberline {2.1.8}mapping(address $=>$ uint256[]) private loansFunder}{7}{subsubsection.2.1.8}
\contentsline {subsubsection}{\numberline {2.1.9}mapping(address $=>$ uint256[]) private loansGuarantor}{7}{subsubsection.2.1.9}
\contentsline {subsubsection}{\numberline {2.1.10}mapping(uint256 $=>$ uint256[]) private guaranteesProposed}{7}{subsubsection.2.1.10}
\contentsline {subsubsection}{\numberline {2.1.11}mapping(uint256 $=>$ guarantee) private guaranteeReceipt}{7}{subsubsection.2.1.11}
\contentsline {subsection}{\numberline {2.2}Access Modifiers}{7}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}isOwnerOfLoan(loanId)}{8}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}isPaybackValid(loanId)}{8}{subsubsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.3}isGuarantorEligibleRefund(loanId)}{8}{subsubsection.2.2.3}
\contentsline {subsubsection}{\numberline {2.2.4}isFunderEligible(loanId)}{9}{subsubsection.2.2.4}
\contentsline {subsubsection}{\numberline {2.2.5}isFunderAllowed(loanId)}{9}{subsubsection.2.2.5}
\contentsline {subsubsection}{\numberline {2.2.6}isGuaranteeValid(loanId, etherInInterest)}{9}{subsubsection.2.2.6}
\contentsline {subsection}{\numberline {2.3}Generic Functions}{10}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}getFunds()}{10}{subsubsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.2}getEtherRequired(loanId)}{10}{subsubsection.2.3.2}
\contentsline {subsubsection}{\numberline {2.3.3}getTotalToPay(loanId)}{10}{subsubsection.2.3.3}
\contentsline {subsubsection}{\numberline {2.3.4}getTotalUnfundedLoans()}{11}{subsubsection.2.3.4}
\contentsline {subsubsection}{\numberline {2.3.5}getUnfundedLoanDetails(index)}{11}{subsubsection.2.3.5}
\contentsline {subsubsection}{\numberline {2.3.6}getCurrentTime()}{11}{subsubsection.2.3.6}
\contentsline {subsubsection}{\numberline {2.3.7}deleteLoanIdFromArray(loanId, array)}{11}{subsubsection.2.3.7}
\contentsline {subsubsection}{\numberline {2.3.8}generateId()}{12}{subsubsection.2.3.8}
\contentsline {subsubsection}{\numberline {2.3.9}deleteContract()}{12}{subsubsection.2.3.9}
\contentsline {subsection}{\numberline {2.4}Borrower Functions}{13}{subsection.2.4}
\contentsline {subsubsection}{\numberline {2.4.1}depositLoanRequest(etherRequested, etherInInterest, daysToPay)}{13}{subsubsection.2.4.1}
\contentsline {subsubsection}{\numberline {2.4.2}getNumberLoans()}{13}{subsubsection.2.4.2}
\contentsline {subsubsection}{\numberline {2.4.3}getLoanIdBorrower(index)}{13}{subsubsection.2.4.3}
\contentsline {subsubsection}{\numberline {2.4.4}getTotalGuarantees(loanId)}{14}{subsubsection.2.4.4}
\contentsline {subsubsection}{\numberline {2.4.5}getGuaranteeReceipt(loanId, index)}{14}{subsubsection.2.4.5}
\contentsline {subsubsection}{\numberline {2.4.6}acceptGuarantee(loanId, guaranteeId)}{14}{subsubsection.2.4.6}
\contentsline {subsubsection}{\numberline {2.4.7}payLoanBack(loanId)}{15}{subsubsection.2.4.7}
\contentsline {subsection}{\numberline {2.5}Guarantor Functions}{16}{subsection.2.5}
\contentsline {subsubsection}{\numberline {2.5.1}depositGuarantee(loanId, etherInInterest)}{17}{subsubsection.2.5.1}
\contentsline {subsubsection}{\numberline {2.5.2}getRefundGuarantor(loanId)}{17}{subsubsection.2.5.2}
\contentsline {subsubsection}{\numberline {2.5.3}getNumberGuarantees()}{18}{subsubsection.2.5.3}
\contentsline {subsubsection}{\numberline {2.5.4}getLoanIdGuarantor(index)}{18}{subsubsection.2.5.4}
\contentsline {subsection}{\numberline {2.6}Lender Functions}{18}{subsection.2.6}
\contentsline {subsubsection}{\numberline {2.6.1}lendMoney(loanId)}{18}{subsubsection.2.6.1}
\contentsline {subsubsection}{\numberline {2.6.2}getRefundFunder(loanId)}{19}{subsubsection.2.6.2}
\contentsline {subsubsection}{\numberline {2.6.3}getNumberFunds()}{19}{subsubsection.2.6.3}
\contentsline {subsubsection}{\numberline {2.6.4}getLoanIdFunder(index)}{20}{subsubsection.2.6.4}
\contentsline {subsection}{\numberline {2.7}Design Patterns Used and Bugs Avoided}{20}{subsection.2.7}
\contentsline {subsection}{\numberline {2.8}Environment Setup}{21}{subsection.2.8}
\contentsline {section}{\numberline {3}Testing Of Smart Contract}{22}{section.3}
\contentsline {subsection}{\numberline {3.1}Tests: acceptGuarantee()}{22}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Valid Guarantee -- Loan request status updated appropriately}{22}{subsubsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.2}Valid Guarantee -- When borrower accepts guarantee, guarantee proposals for loan are deleted}{23}{subsubsection.3.1.2}
\contentsline {subsubsection}{\numberline {3.1.3}When Borrower accepts guarantee, guarantor has guarantee added to his list.}{24}{subsubsection.3.1.3}
\contentsline {subsubsection}{\numberline {3.1.4}When Borrower accepts guarantee, guarantors having guarantees rejected get refund.}{24}{subsubsection.3.1.4}
\contentsline {subsubsection}{\numberline {3.1.5}Should Test: When Borrower accepts guarantee, he is the actual owner of loan request.}{25}{subsubsection.3.1.5}
\contentsline {subsubsection}{\numberline {3.1.6}When Borrower tries to accept a guarantee, loan request does not have a guarantor already}{26}{subsubsection.3.1.6}
\contentsline {subsubsection}{\numberline {3.1.7}Should Test: When Borrower tries to accept a guarantee, loan request does not have a funder already}{27}{subsubsection.3.1.7}
\contentsline {subsubsection}{\numberline {3.1.8}When Borrower tries to accept a guarantee, loan request must have at least one guarantee proposal}{28}{subsubsection.3.1.8}
\contentsline {subsection}{\numberline {3.2}Tests: depositGuarantee()}{28}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}When Guarantor deposits Guarantee, guarantee proposal of particular loanRequest increases and guarantee struct is saved in map}{28}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}When Guarantor deposits Guarantee, loan request must not be already funded}{29}{subsubsection.3.2.2}
\contentsline {subsubsection}{\numberline {3.2.3}When Guarantor deposits Guarantee, loan request must not already have a guarantee}{30}{subsubsection.3.2.3}
\contentsline {subsubsection}{\numberline {3.2.4}When Guarantor deposits Guarantee, he sends sufficient funds}{30}{subsubsection.3.2.4}
\contentsline {subsubsection}{\numberline {3.2.5}When Guarantor deposits Guarantee, the requested interest is not greater than the maximum offerred by borrower}{31}{subsubsection.3.2.5}
\contentsline {subsubsection}{\numberline {3.2.6}When Guarantor deposits Guarantee, loan request must not be already overdue}{31}{subsubsection.3.2.6}
\contentsline {subsubsection}{\numberline {3.2.7}When Guarantor deposits Guarantee, money is saved in contract fund}{32}{subsubsection.3.2.7}
\contentsline {subsection}{\numberline {3.3}Tests: depositLoanRequest()}{33}{subsection.3.3}
\contentsline {subsubsection}{\numberline {3.3.1}Valid Loan Request - Loan added to unfundedLoans and array}{33}{subsubsection.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.2}Valid Loan Request - Loan added to list of loan request borrower possesses}{33}{subsubsection.3.3.2}
\contentsline {subsection}{\numberline {3.4}Tests: lendMoney()}{33}{subsection.3.4}
\contentsline {subsubsection}{\numberline {3.4.1}When Lender deposits Money, enough funds are sent}{33}{subsubsection.3.4.1}
\contentsline {subsubsection}{\numberline {3.4.2}When Lender deposits Money, money is sent to borrower}{34}{subsubsection.3.4.2}
\contentsline {subsubsection}{\numberline {3.4.3}When Lender deposits Money, loan request is removed from unfunded}{34}{subsubsection.3.4.3}
\contentsline {subsubsection}{\numberline {3.4.4}When Lender deposits Money, fund be added to funder list}{35}{subsubsection.3.4.4}
\contentsline {subsubsection}{\numberline {3.4.5}When Lender tries to deposit Money, he gets stopped if loan request is already overdue}{36}{subsubsection.3.4.5}
\contentsline {subsubsection}{\numberline {3.4.6}When Lender tries to deposit Money, he gets stopped if loan request is already funded}{36}{subsubsection.3.4.6}
\contentsline {subsection}{\numberline {3.5}Tests: payLoanBack()}{37}{subsection.3.5}
\contentsline {subsubsection}{\numberline {3.5.1}When Borrower tries to pay back loan, if amount of funds he sends arent enough to cover loan and interest, he is stopped}{37}{subsubsection.3.5.1}
\contentsline {subsubsection}{\numberline {3.5.2}When Borrower tries to pay back loan, but loan is not his, he is stopped}{38}{subsubsection.3.5.2}
\contentsline {subsubsection}{\numberline {3.5.3}When Borrower tries to pay back loan, all parties involved are paid appropriately}{38}{subsubsection.3.5.3}
\contentsline {subsubsection}{\numberline {3.5.4}When Borrower tries to pay back loan, loan gets deleted from record of participants}{39}{subsubsection.3.5.4}
\contentsline {subsection}{\numberline {3.6}Tests: getRefund() and deleteContract()}{40}{subsection.3.6}
\contentsline {subsection}{\numberline {3.7}Environment Setup}{40}{subsection.3.7}
\contentsline {subsection}{\numberline {3.8}Testing Outcome}{41}{subsection.3.8}
\contentsline {section}{\numberline {4}Front End Design}{41}{section.4}
\contentsline {subsection}{\numberline {4.1}System In Operation}{46}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1}Borrower Accesses His Page}{46}{subsubsection.4.1.1}
\contentsline {subsubsection}{\numberline {4.1.2}Borrower Makes First Loan Request}{46}{subsubsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.3}Borrower Makes Some More Loan Requests}{47}{subsubsection.4.1.3}
\contentsline {subsubsection}{\numberline {4.1.4}Borrower Checks His Loan Request Id's}{48}{subsubsection.4.1.4}
\contentsline {subsubsection}{\numberline {4.1.5}Guarantor Checks List Of Unfunded Loans}{48}{subsubsection.4.1.5}
\contentsline {subsubsection}{\numberline {4.1.6}Guarantor Deposits Two Guarantees}{49}{subsubsection.4.1.6}
\contentsline {subsubsection}{\numberline {4.1.7}Borrower Checks Guarantee Proposals On A Loan}{50}{subsubsection.4.1.7}
\contentsline {subsubsection}{\numberline {4.1.8}Borrower Accepts Guarantee}{50}{subsubsection.4.1.8}
\contentsline {subsubsection}{\numberline {4.1.9}Lender Checks List Of Unfunded Transactions}{51}{subsubsection.4.1.9}
\contentsline {subsubsection}{\numberline {4.1.10}Lender Funds Loan}{51}{subsubsection.4.1.10}
\contentsline {subsubsection}{\numberline {4.1.11}Borrower Recieves Loan And Pays It Back}{52}{subsubsection.4.1.11}
\contentsline {subsubsection}{\numberline {4.1.12}After Affects After Paying Loan Back}{52}{subsubsection.4.1.12}
\contentsline {subsection}{\numberline {4.2}Setup Summary}{53}{subsection.4.2}
